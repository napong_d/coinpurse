package coinpurse;
/**
 * interface of Valuable.
 * @author Napong Dungduangsasitorn.
 */

public interface Valuable extends Comparable<Valuable>{
	
	/**
	 * get value of object.
	 * @return value of object.
	 */
	public double getValue();
	
}


