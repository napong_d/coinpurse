package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;

/**
 * recursive withdraw.
 * @author Napong Dungduangsasitorn
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy {

	/**
	 * withdraw by use recursive.
	 * @param amount is amount of money to withdraw.
	 * @param valuableList list of item in purse.
	 * @return item of withdraw or null if can't withdraw.
	 */
	@Override
	public Valuable[] withdraw(double amount, List<Valuable> valuableList) {
		Collections.sort(valuableList);
		List<Valuable> list = withdrawRecur(amount,valuableList,valuableList.size()-1);
		if(list == null){
			return null;
		}
		return list.toArray(new Valuable[0]);
	}


	/**
	 * withdraw method for use recursive.
	 * @param amount amount is amount of money to withdraw.
	 * @param valuableList list of item in purse.
	 * @param index is index list use for runner. 
	 * @return list of valuables or return null if can't withdraw.
	 */
	public List<Valuable> withdrawRecur(double amount, List<Valuable> valuableList, int index ){
		if(index < 0) return null;

		if(amount == 0){
			List<Valuable> tempList = new ArrayList<Valuable>();
			return tempList;
		}
		double value = valuableList.get(index).getValue();
		if(amount < value){
			return null;
		}
		
		if(amount - value == 0){
			List<Valuable> returnMoney = new ArrayList<Valuable>();
			returnMoney.add(valuableList.get(index));
			return returnMoney;
		}
		
		List<Valuable> returnWithdraw = withdrawRecur(amount, valuableList, index-1);
		if(returnWithdraw == null){
			returnWithdraw = withdrawRecur(amount, valuableList, index-1);
		}
		else{
			returnWithdraw.add(valuableList.get(index));
		}

		return returnWithdraw;

	}
}
