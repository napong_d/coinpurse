package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
import coinpurse.ValueComparator;

/**
 * GreedyWithdraw use greedy algorithm to withdraw.
 * @author Napong Dungduangsasitorn
 *
 */
public class GreedyWithdraw implements WithdrawStrategy{
	ValueComparator valuecomparator = new ValueComparator();


	/**
	 * @param amount to withdraw.
	 * @param valuables of value.
	 * @return array of value.
	 */
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		List<Valuable> temp = new ArrayList<Valuable>();
		for(int i = valuables.size() - 1 ; i >= 0 ; i--){
			if(amount > 0){
				if(valuables.get(i).getValue() <= 0){
					amount -= valuables.get(i).getValue();
					temp.add(valuables.get(i));
				}
			}
		}

		if(amount > 0){
			return null;
		}

		return temp.toArray(new Valuable[0]);
	}



}
