package coinpurse;

import org.omg.CORBA.Object;

/**
 * Banknote object.
 * @author Napong Dungduangsasitorn
 *
 */
public class BankNote extends AbstractValuable {

	private double value;
	private static int nextSerialNumber = 1000000;
	private int currentSerial;
	/**
	 * contructor of banknote.
	 * @param value of banknote.
	 */
	public BankNote (double value){
		super(value);
		this.currentSerial = nextSerialNumber;
		this.nextSerialNumber++;
	}


	/**
	 * get next serial number of banknote.
	 * @return nextSerialNumber.
	 */
	public static int getNextSerialNumber(){
		return nextSerialNumber++;
	}
	
	
	/**
	 * print value and serial number of banknote. 
	 * @return string serial of banknote.
	 */
	public String toString(){
		return getValue()+"-Baht Banknote ["+ this.currentSerial + "]";
	}


}
