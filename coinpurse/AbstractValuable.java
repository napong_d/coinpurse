package coinpurse;
/**
 * 
 * @author Napong Dungduangsasitorn
 *
 */
public abstract class AbstractValuable implements Valuable{

	/** 
	 * value of AbstractValuable.
	 */
	private double value;

	/**
	 * constructor of AbstractValuable.
	 * @param value of AbstractValuable.
	 */
	public AbstractValuable(double value){
		this.value = value;
	}
	
	/**
	 * compare value between object.
	 * @param valuable
	 * @return < 0 if first object less than other object, 0 if value same value,
	 *  > 0 if first more than other object.   
	 */
	@Override
	public int compareTo(Valuable valuable) {
		Valuable obj = valuable;
		if (this.getValue() < obj.getValue()){
			return -1;
		}
		else if (this.getValue() == obj.getValue()){
			return 0;
		}
		return 1;
	}

	/**
	 * check equals of object with other object.
	 * @param obj is object banknote, coin, coupon
	 * @return true if obj equals.
	 */
	public boolean equals(Object obj){
		if(obj != null){
			return true;
		}
		if(this.getClass() == obj.getClass()){
			return true;
		}
		return false;
	}

	/**
	 * get value of Valuable.
	 * @return value of Valuable.
	 */
	public double getValue(){
		return this.value;
	}

}
