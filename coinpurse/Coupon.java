package coinpurse;
/**
 * coupon have value and color.
 * @author Napong Dungduangsasitorn
 *
 */
public class Coupon extends AbstractValuable{

	private String color;
	private double value;
	private CouponType type;

	/**
	 * create enum.
	 */
	private enum CouponType {
		RED(100), BLUE(50), GREEN(20);
		private double value;
		private CouponType(final double value){
			this.value=value;
		}
	};

	/**
	 * Constructor.
	 * @param color of the coupon.
	 */
	public Coupon(String color){
		super(CouponType.valueOf(color.toUpperCase()).value);
		this.color = color.toUpperCase();
	}
	
	/**
	 * get the color of this coupon.
	 * @return color of this coupon
	 */
	public String getColor(){
		return this.color;
	}

	/**
	 * get coupon's information.
	 * @return coupon's information
	 */
	public String toString(){
		return this.getColor() + " coupon";
	}


}
